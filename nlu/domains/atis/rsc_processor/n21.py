'''
ATIS N21 specific
    - resource related codes

Author : Sangkeun Jung (2019)
'''

import numpy as np
import random
import codecs

def load_rsc(fn):
    # INPUT : id converted tab seperated format 
    print("Data loading from {}".format(fn))

    rsc = []
    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip()
            fields = line.split('\t')

            n21_class_id = int( fields[0] )
            token_ids = [ int(_id) for _id in fields[1:] ]

            rsc.append( (n21_class_id, token_ids) ) 

    return rsc 

def line_tensor_process(tokens, num_steps, pad_token_id=1, reverse=True):
    # input: id converted tokens
    token_ids = [ pad_token_id ] * num_steps
    weight    = [ 0 ] * num_steps

    for idx, _id in enumerate(tokens):
        token_ids[idx] = _id
        weight[idx] = 1 

    # why reverse?? --> for padding symbol processing
    if reverse == True:
        token_ids = list( reversed( token_ids ) ) 
        weight    = list( reversed( weight ) )    

    return token_ids, weight
          

from random import randint
def make_tensor_data(rsc, num_steps, batch_size, pad_token_id=1):
    num_exs = len(rsc) 

    n_to_fill = batch_size - num_exs % batch_size
    N = num_exs + n_to_fill

    # vectorize id data
    n_intent  = np.zeros([N],            np.int64)  
    n_token   = np.zeros([N, num_steps], np.int64)
    n_weight  = np.zeros([N, num_steps], np.int64)

    for ex_id, (intent_id, tokens) in enumerate(rsc):
        # for tokens
        token_ids, weight = line_tensor_process(tokens, num_steps, pad_token_id, reverse=True)
            
        # store to numpy array
        n_intent[ex_id]  = intent_id # shape=(4478,)
        n_token[ex_id]   = token_ids
        n_weight[ex_id]  = weight

    # 3) fill remaining batch
    random_idxs = [randint(0, num_exs-1) for p in range(0, n_to_fill)] 
    for _idx, random_index in enumerate( random_idxs ):
        idx = num_exs + _idx
        n_intent[idx]  = n_intent[random_index]
        n_token[idx]   = n_token[random_index] 
        n_weight[idx]  = n_weight[random_index] 

    tensors = n_intent, n_token, n_weight
    return tensors
    
def make_batch_data(rsc, batch_size, shuffle=True):
    # chunk the resource and make it as batch form
    #
    # rsc = list of (n_intent, n_token, n_weight)
    
    n_intent, n_token, n_weight = rsc 
    num_exs = n_intent.shape[0]
    indices = list( range(0, num_exs) )

    if shuffle == True:
        random.shuffle(indices)

    # batches
    batches = []
    for i in range(0, len(indices), batch_size):
        a_batch_idx = indices[i:i+batch_size]
        a_batch_intent, a_batch_token, a_batch_weight = rsc[0][a_batch_idx], rsc[1][a_batch_idx], rsc[2][a_batch_idx]
        batches.append( (a_batch_intent, a_batch_token, a_batch_weight) )

    return batches