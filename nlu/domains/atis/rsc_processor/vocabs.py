'''
ATIS N21 specific
    - resource related codes

Author : Sangkeun Jung (2019)
'''

import codecs

def load_vocab(fn):
    print("Vocab loading from {}".format(fn))

    vocab = {}
    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip()
            symbol, _id = line.split('\t')
            vocab[symbol] =  int(_id) 

    return vocab 

def load_vocabs(fn_dict):

    target_vocabs = ['token', 'n21', 'n2n']

    vocabs = {}
    for target in target_vocabs:
        vocab = load_vocab( fn_dict[target] )
        vocabs[target] = vocab 

    return vocabs 
