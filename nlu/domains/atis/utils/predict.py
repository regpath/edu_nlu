"""
    Train Utils (ATIS)
        - Intent Analysis

    Author : Sangkeun Jung (hugmanskj@gmail.com), 2019
"""
import torch 
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np 

def load_model(model_fn):
    return torch.load(model_fn)

def test_n21(model, batch_data, actual_num_exs):
    # batch_data is filled with randomly selected exs to match BATCH_SIZE SHAPE
    # to ignore the randomly-filled exs, acutal num exs are used. 

    model.eval() # set information to pytorch that the current mode is 'testing'
    device = torch.device("cuda" if model.hps.use_gpu else "cpu")

    all_predicts   = []
    all_references = []
    for idx, a_batch_data in enumerate(batch_data):
        b_intent_id, b_token_ids, b_weight = a_batch_data
            
        # convert numpy to pytorch
        b_intent_id  = Variable( torch.from_numpy(b_intent_id) ).to(device)
        b_token_ids  = Variable( torch.from_numpy(b_token_ids) ).to(device)
        b_weight     = Variable( torch.from_numpy(b_weight) ).to(device)

        # forward pass
        predicts = model(b_token_ids, b_weight)
        logit = predicts.data.cpu().numpy()
        pred_idxs = np.argmax(logit, axis=1)

        for p in pred_idxs:      all_predicts.append(p)
        for r in b_intent_id:    all_references.append(r)

    # calculate the accuracy
    num_corrects = 0

    # ignore randomly filled exs 
    all_predicts   = all_predicts[:actual_num_exs] 
    all_references = all_references[:actual_num_exs] 
    for p,r in zip(all_predicts, all_references):
        if p == r : num_corrects += 1

    accuracy = float(num_corrects) / float( len(all_predicts) )
    print("Accuracy of the model on testing data : {:.6f}".format(accuracy))

import codecs

def test_n2n(model, batch_data, to_result_fn, actual_num_exs):
    model.eval() # set information to pytorch that the current mode is 'testing'
    device = torch.device("cuda" if model.hps.use_gpu else "cpu")

    result_lines = []
    for idx, a_batch_data in enumerate(batch_data):
        b_target_ids, b_token_ids, b_weight = a_batch_data
        ref_target_ids = b_target_ids
            
        # convert numpy to pytorch
        b_target_ids  = Variable( torch.from_numpy(b_target_ids) ).to(device)
        b_token_ids   = Variable( torch.from_numpy(b_token_ids) ).to(device)

        # feed-forward (inference)
        logits           = model(b_token_ids)  # logits = [batch_size, num_steps, num_target_class]

        # for simplicity 
        step_result = []
        for idx, a_sent in enumerate(logits):
            for step_logit in a_sent: # a_sent : [num_steps, num_target_class]
                step_probs = F.softmax(step_logit)
                values, indices = torch.max(step_probs, 0)

                best_index = int( indices.data.cpu().numpy() )
                best_prob  = float( values.data.cpu().numpy() )
                
                step_result.append( (best_index, best_prob ) )

            # to result format
            weights = b_weight[idx]
            items = []
            for t_idx, weight in enumerate( weights ):
                if weight != 1: continue 
                best_class_index, best_class_prob = step_result[t_idx]
                item = "{},{}".format(best_class_index, best_class_prob)
                items.append( item )                
            a_line = "\t".join( items ) 
            result_lines.append( a_line )

    # ignore randomly filled exs 
    result_lines   = result_lines[:actual_num_exs] 
    with codecs.open(to_result_fn, 'w', encoding='utf-8') as f:
        for line in result_lines:
            print(line, file=f)

    print("Prediction result is dumped at {}".format(to_result_fn) )
          
    
def test_crf_n2n(model, batch_data, to_result_fn, actual_num_exs, fast_eval=True):
    ## fast_eval == True ? --> fast decoding with batch_size = large --> not correct prob
    ## fast_eval == False  --> slow decoding with batch_size = 1 --> correct sequence-wise prob
    ## this is to do list. 

    model.eval() # set for evluation mode
    if fast_eval == False:
        batch_size = 1
        model.set_batch_size(batch_size) # for evaluation mode

    device = torch.device("cuda" if model.hps.use_gpu else "cpu")

    result_lines = []
    for idx, a_batch_data in enumerate(batch_data):
        b_target_ids, b_token_ids, b_weight = a_batch_data
        ref_target_ids = b_target_ids
            
        # convert numpy to pytorch
        b_target_ids  = Variable( torch.from_numpy(b_target_ids) ).to(device)
        b_token_ids   = Variable( torch.from_numpy(b_token_ids) ).to(device)
        b_weight      = Variable( torch.from_numpy(b_weight) ).to(device)

        # feed-forward (inference)
        logits           = model(b_token_ids)  # logits = [batch_size, num_steps, num_target_class]

        # pred_tag_ids : a list of list ( list for masked tag ids ) <- BATCH_SIZE
        # prob_paths   : a numpy list path prob <- BATCH_SIZE
        pred_tag_ids, prob_paths = model.predict_and_get_prob(logits, b_weight) 

        for idx, (a_pred_seq, a_seq_pred_prob) in enumerate( zip(pred_tag_ids, list(prob_paths) )):
            best_class_prob = a_seq_pred_prob # Not Available step-wise prob. in CRF mode. Just use sequence prob for now
            items = []
            for best_class_index in a_pred_seq: # a_sent : [num_steps, num_target_class]
                item = "{},{}".format(best_class_index, best_class_prob)
                items.append( item ) 
            a_line = "\t".join( items )
            result_lines.append( a_line )

    # ignore randomly filled exs 
    result_lines   = result_lines[:actual_num_exs] 
    with codecs.open(to_result_fn, 'w', encoding='utf-8') as f:
        for line in result_lines:
            print(line, file=f)

    print("Prediction result is dumped at {}".format(to_result_fn) )


    