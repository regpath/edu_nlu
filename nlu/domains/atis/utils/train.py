"""
    Train Utils (ATIS)
        - Intent Analysis

    Author : Sangkeun Jung (hugmanskj@gmail.com), 2019
"""
import torch 
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np 

def train_n21(model, batch_data, to_model_fn):
    # train loop 
    model.train() # set information to pytorch that the current mode is 'training'
    
    # use gpu if set
    device = torch.device("cuda" if model.hps.use_gpu else "cpu")
    if device == 'cuda': model = model.cuda()

    # Loss and Optimizer
    criterion = nn.CrossEntropyLoss()  
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)  

    # training loop
    step = 0
    avg_losses = []
    for epoch in range(model.hps.max_epoch):
        for idx, a_batch_data in enumerate(batch_data):
            b_intent_id, b_token_ids, b_weight = a_batch_data

            # convert numpy to pytorch
            b_intent_id  = Variable( torch.from_numpy(b_intent_id) ).to(device)
            b_token_ids  = Variable( torch.from_numpy(b_token_ids) ).to(device)
            b_weight     = Variable( torch.from_numpy(b_weight) ).to(device)

            # init for updating the current batch
            optimizer.zero_grad()

            # forward pass
            logit = model(b_token_ids, b_weight)

            # loss calculation
            loss  = F.cross_entropy(logit, b_intent_id) 
            
            # backward pass
            loss.backward() # accumulates the gradient (by addition) for each parameter. This is why you should call optimizer.zero_grad()

            # parameter update 
            optimizer.step()

            # monitoring at every 100 steps
            _loss = loss.item()  # loss.item() # gets the a scalar value held in the loss.
            avg_losses.append( _loss ) 
            if step % 100 == 0 :
                print('Epoch={} \t Step={} \t Loss={:.6f}'.format(
                            epoch, 
                            step,
                            np.mean(avg_losses)
                        )
                      )
                avg_losses = []
            step += 1

    # save model 
    torch.save(model, to_model_fn)
    print("Model saved at {}".format(to_model_fn) )


from common.ml.masked_cross_entropy import compute_loss as sequence_loss 
def train_n2n(model, batch_data, to_model_fn):
    model.train() # set information to pytorch that the current mode is 'training'

    # use gpu if set
    device = torch.device("cuda" if model.hps.use_gpu else "cpu")
    if device == 'cuda': model = model.cuda()

    # Loss and Optimizer
    criterion = nn.CrossEntropyLoss()  
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)  

    # training loop
    step = 0
    avg_losses = []
    for epoch in range(model.hps.max_epoch):
        for idx, a_batch_data in enumerate(batch_data):
            b_target_ids, b_token_ids, b_weight = a_batch_data

            # convert numpy to pytorch
            b_target_ids = Variable( torch.from_numpy(b_target_ids) ).to(device)
            b_token_ids  = Variable( torch.from_numpy(b_token_ids) ).to(device)
            b_n_weight   = np.sum(b_weight, axis=1) # [ [1,1,1,1,0], [1,1,0,0,0] ] --> [ [4],[2] ]
            b_n_weight   = Variable( torch.from_numpy(b_n_weight).type(torch.LongTensor) ).to(device)


            # init for updating the current batch
            optimizer.zero_grad()

            # forward pass
            logits = model(b_token_ids)

            # loss calculation considering padding symbols
            loss = sequence_loss(logits, b_target_ids, b_n_weight)
            
            # backward pass
            loss.backward() # accumulates the gradient (by addition) for each parameter. This is why you should call optimizer.zero_grad()

            # parameter update 
            optimizer.step()

            # monitoring at every 100 steps
            _loss = loss.item()  # loss.item() # gets the a scalar value held in the loss.
            avg_losses.append( _loss ) 
            if step % 100 == 0 :
                print('Epoch={} \t Step={} \t Loss={:.6f}'.format(
                            epoch, 
                            step,
                            np.mean(avg_losses)
                        )
                      )
                avg_losses = []
            step += 1

    # save model 
    torch.save(model, to_model_fn)
    print("Model saved at {}".format(to_model_fn) )


def train_crf_n2n(model, batch_data, to_model_fn):
    model.train() # set information to pytorch that the current mode is 'training'
    
    # use gpu if set
    device = torch.device("cuda" if model.hps.use_gpu else "cpu")
    if device.type == 'cuda': model = model.cuda()


    # Loss and Optimizer
    criterion = nn.CrossEntropyLoss()  
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)  

    # training loop
    step = 0
    avg_losses = []

    for epoch in range(model.hps.max_epoch):
        for idx, a_batch_data in enumerate(batch_data):
            b_target_ids, b_token_ids, b_weight = a_batch_data

            # convert numpy to pytorch
            b_target_ids = Variable( torch.from_numpy(b_target_ids) ).to(device)
            b_token_ids  = Variable( torch.from_numpy(b_token_ids) ).to(device)
            b_weight     = Variable( torch.from_numpy(b_weight) ).to(device)

            # init for updating the current batch
            optimizer.zero_grad()

            # forward pass
            logits = model(b_token_ids)

            # loss calculation considering padding symbols
            loss = model.get_crf_loss(logits, b_target_ids, b_weight)

            # backward pass
            loss.backward() # accumulates the gradient (by addition) for each parameter. This is why you should call optimizer.zero_grad()

            # parameter update
            optimizer.step()

            # monitoring at every 100 steps
            _loss = loss.item()  # loss.item() # gets the a scalar value held in the loss.
            avg_losses.append( _loss ) 
            if step % 10 == 0 :
                print('Epoch={} \t Step={} \t Loss={:.6f}'.format(
                            epoch, 
                            step,
                            np.mean(avg_losses)
                        )
                      )
                avg_losses = []
            step += 1

    # save model 
    torch.save(model, to_model_fn)
    print("Model saved at {}".format(to_model_fn) )