'''
Neural Network for mnist 

Author : Sangkeun Jung (2019)
'''

import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F

class Sequence_RNN_Layer(nn.Module):
    def __init__(self, input_dim, hidden_dim, batch_size, num_layer, cell_type='LSTM', birnn=False, use_gpu=True):
        super(Sequence_RNN_Layer, self).__init__()
        # input_dim     : dimension for each input
        # num_steps     : number of token in a sequence
        # num_layers    : number of layers of RNN
        # cell_type     : 'LSTM', 'GRU'

        self.hidden_dim     = hidden_dim
        self.use_gpu        = use_gpu
        self.num_rnn_layers = num_layer
        self.batch_size     = batch_size
        
        self.birnn          = birnn 
        self.num_numerator  = 2 if self.birnn else 1 

        if cell_type == 'LSTM':
            self.rnn = nn.LSTM(input_dim, 
                               self.hidden_dim // self.num_numerator, 
                               num_layers=self.num_rnn_layers, 
                               bidirectional=self.birnn,
                               batch_first=True
                              ) # [batch_size, input_dim] --> [batch_size, hidden_dim]
        if cell_type == 'GRU':
            print("NEED TO IMPLEMENT")
            pass 

        self.reset_initial_state()
        
    def init_hidden(self):
        if self.birnn: 
            _num_rnn_layers = self.num_rnn_layers*2
        else: 
            _num_rnn_layers = self.num_rnn_layers

        if self.use_gpu:
            h0 = Variable(torch.zeros(_num_rnn_layers, self.batch_size, self.hidden_dim // self.num_numerator).cuda())
            c0 = Variable(torch.zeros(_num_rnn_layers, self.batch_size, self.hidden_dim // self.num_numerator).cuda())
        else:
            h0 = Variable(torch.zeros(_num_rnn_layers, self.batch_size, self.hidden_dim // self.num_numerator))
            c0 = Variable(torch.zeros(_num_rnn_layers, self.batch_size, self.hidden_dim // self.num_numerator))
        return (h0, c0)

    def reset_initial_state(self):
        self.initial_state = self.init_hidden() 

    def set_batch_size(self, batch_size):
        self.batch_size = batch_size

    
    def forward(self, input):   
        # input = [batch_size, num_steps, input_dim]
        #
        # return rnn_output and last output 
        
        self.reset_initial_state()
        rnn_out, self.initial_state = self.rnn(input, self.initial_state)
        # rnn_out = [batch_size, num_steps, output_dim]
        
        last_rnn_out = rnn_out[:,-1,:]
        # output 
        #   - rnn_out : [batch_size, num_steps, output_dim]  
        #   - last_rnn_out : [batch_size, output_dim]
        return rnn_out, last_rnn_out


class lstm_n21(nn.Module):
    def __init__(self, hps):
        super(lstm_n21, self).__init__()

        self.hps = hps 
        self.batch_size = hps.batch_size 

        self.word_embeddings = nn.Embedding(hps.num_token_vocabs, hps.token_emb_dim)   # [batch_size, emb_dim]
        self.encoder = Sequence_RNN_Layer(  hps.token_emb_dim,              # dim of input for rnn encoder
                                            hps.intent_dim,                 # dim of intent vector
                                            batch_size=self.batch_size,
                                            num_layer=1,                    # number of rnn layers 
                                            cell_type='LSTM', 
                                            birnn=False,
                                            use_gpu=False, # cpu only now
                                         )

        self.to_intent_tag = nn.Linear(hps.intent_dim, hps.num_intent_tags)  # [batch_size, sf_dim] --> [batch_size, num_intent_tag]    


    def forward(self, b_token_ids, b_weight):
        # embedding
        embedding = self.word_embeddings(b_token_ids) # [batch_size, num_steps, emb_dim]
        embedding = F.dropout(embedding, p=self.hps.keep_prob, training=self.training)

        # embedding -> RNN layer
        rnn_output, last_rnn_output = self.encoder(embedding) # [batch_size, num_steps, emb_dim] -> [batch_size, num_steps, enc_dim]
        out = last_rnn_output

        # lstm output --> # of intent classes
        logit_v_intent = self.to_intent_tag(out)

        return logit_v_intent
    
    def name(self):
        return "LSTM intent encoder"


class lstm_n2n(nn.Module):
    def __init__(self, hps):
        super(lstm_n2n, self).__init__()

        self.hps = hps 
        self.batch_size = hps.batch_size 

        self.word_embeddings = nn.Embedding(hps.num_token_vocabs, hps.token_emb_dim)   # [batch_size, emb_dim]
        self.encoder = Sequence_RNN_Layer(  hps.token_emb_dim,              # dim of input for rnn encoder
                                            hps.slot_dim,                 # dim of semantic frame 
                                            batch_size=self.batch_size,
                                            num_layer=1,                    # number of rnn layers 
                                            cell_type='LSTM', 
                                            birnn=False,
                                            use_gpu=False, # cpu only now
                                         )

        self.to_class = nn.Linear(hps.slot_dim, hps.num_slot_tags)  # [batch_size, out_dim] --> [batch_size, num_target_class]    

    def set_batch_size(self, batch_size):
        self.batch_size = batch_size
        self.encoder.set_batch_size(batch_size)

    def forward(self, b_token_ids):
        # embedding
        embedding = self.word_embeddings(b_token_ids) # [batch_size, num_steps, emb_dim]
        embedding = F.dropout(embedding, p=self.hps.keep_prob, training=self.training)

        # embedding -> RNN layer
        rnn_output , last_rnn_output = self.encoder(embedding) # [batch_size, num_steps, emb_dim] -> [batch_size, num_steps, enc_dim]

        # rnn output -> target class
        logit = self.to_class(rnn_output)    # [batch_size, num_steps, num_target_class]

        return logit
    
    def name(self):
        return "LSTM N2N encoder"



from common.nn.conditional_random_field import ConditionalRandomField
#, viterbi_decode

class lstm_crf_n2n(nn.Module):
    def __init__(self, hps):
        super(lstm_crf_n2n, self).__init__()
        self.hps = hps 
        self.batch_size = hps.batch_size 

        self.word_embeddings = nn.Embedding(hps.num_token_vocabs, hps.token_emb_dim)   # [batch_size, emb_dim]
        self.encoder = Sequence_RNN_Layer(  hps.token_emb_dim,              # dim of input for rnn encoder
                                            hps.slot_dim,                 # dim of semantic frame 
                                            batch_size=self.batch_size,
                                            num_layer=1,                    # number of rnn layers 
                                            cell_type='LSTM', 
                                            birnn=False,
                                            use_gpu=hps.use_gpu,
                                         )

        self.to_class = nn.Linear(hps.slot_dim, hps.num_slot_tags)  # [batch_size, out_dim] --> [batch_size, num_target_class] 

        # define CRF layer
        self.crf = ConditionalRandomField(hps.num_slot_tags)

    def set_batch_size(self, batch_size):
        self.batch_size = batch_size
        self.encoder.set_batch_size(batch_size)

    def forward(self, b_token_ids):
        # embedding
        embedding = self.word_embeddings(b_token_ids) # [batch_size, num_steps, emb_dim]
        embedding = F.dropout(embedding, p=self.hps.keep_prob, training=self.training)

        # embedding -> RNN layer
        rnn_output , last_rnn_output = self.encoder(embedding) # [batch_size, num_steps, emb_dim] -> [batch_size, num_steps, enc_dim]

        # rnn output -> target class
        logit = self.to_class(rnn_output)    # [batch_size, num_steps, num_target_class]

        # do crf processing on top of results
        #best_path = self.crf(logit)

        return logit

    def get_crf_loss(self, logits, path, weights):
        # logits : [batch_size, num_steps, dim]
        # path   : [batch_size, num_steps], a sequence of given tags
        # weights : [batch_size, num_steps], a sequence of 1 or 0 for marking non-padding and padding symbols

        log_likelihood = self.crf(logits, path, weights)
        negative_log_likelihood = -1.0 * log_likelihood
        loss = negative_log_likelihood
        return loss 

    def predict(self, logits, mask):
        # input : logits = [batch_size, num_steps, num_target_class]
        #         mask   = weights [batch_size, num_steps]
        #
        # do viterbi tags with transition matrix
        #

        predicted_tags = self.crf.viterbi_tags(logits, mask)
        return predicted_tags

    def predict_and_get_prob(self, logits, mask):
        # input : logits = [batch_size, num_steps, num_target_class]
        #         mask   = weights [batch_size, num_steps]
        #
        # do viterbi tags with transition matrix, and get prob
        #
        #predicted_tags, probs = self.crf.viterbi_tags_with_prob(logits.cpu(), mask.cpu())
        predicted_tags, probs = self.crf.viterbi_tags_with_prob(logits, mask)
        return predicted_tags, probs

