"""
    Train Utils

    Author : Sangkeun Jung (hugmanskj@gmail.com), 2019
"""
import torch 
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np 

from torch.utils.data import (DataLoader, RandomSampler, SequentialSampler, TensorDataset)

def train_n21(model, train_exs, hps, to_model_fn):
    # train loop 
    model.train() # set information to pytorch that the current mode is 'training'
    
    # use gpu if set
    device = torch.device("cpu") # default
    if torch.cuda.is_available():
        if model.hps.use_gpu:  
            device = torch.device("cuda")
    if device.type == 'cuda': model = model.cuda()

    # Loss and Optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)  

    # ---- dataset ---- #
    all_label_ids = torch.tensor([f.label_id for f in train_exs], dtype=torch.long)
    all_token_ids = torch.tensor([f.token_ids for f in train_exs], dtype=torch.long)
    all_weight    = torch.tensor([f.weight for f in train_exs], dtype=torch.float)

    train_data = TensorDataset(all_label_ids, all_token_ids, all_weight)
    train_sampler = RandomSampler(train_data)
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=hps.batch_size)

    # training loop
    step = 0
    avg_losses = []
    for epoch in range(hps.max_epoch):
        for step, batch in enumerate(train_dataloader):
            batch = tuple(t.to(device) for t in batch)
            label_id, token_ids, weight = batch

            # init for updating the current batch
            optimizer.zero_grad()

            # forward pass
            loss = model(token_ids, weight, label_id, mode='train')

            # backward pass
            loss.backward() 

            # parameter update 
            optimizer.step()

            # monitoring at every 100 steps
            _loss = loss.item()  # loss.item() # gets the a scalar value held in the loss.
            avg_losses.append( _loss ) 
            if step % 100 == 0 :
                print('Epoch={} \t Step={} \t Loss={:.6f}'.format(
                            epoch, 
                            step,
                            np.mean(avg_losses)
                        )
                      )
                avg_losses = []
            step += 1

    # save model 
    torch.save(model, to_model_fn)
    print("Model saved at {}".format(to_model_fn) )



def train_n2n(model, train_exs, hps, to_model_fn):
    model.train() # set information to pytorch that the current mode is 'training'

    # use gpu if set
    device = torch.device("cpu") # default
    if torch.cuda.is_available():
        if model.hps.use_gpu:  
            device = torch.device("cuda")
    if device.type == 'cuda': model = model.cuda()

    # Loss and Optimizer
    criterion = nn.CrossEntropyLoss()  
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)  

    # training loop
    step = 0
    avg_losses = []

    # ---- dataset ---- #
    all_token_ids = torch.tensor([f.token_ids for f in train_exs], dtype=torch.long)
    all_weight    = torch.tensor([f.weight for f in train_exs], dtype=torch.long)
    all_tag_ids   = torch.tensor([f.tag_ids for f in train_exs], dtype=torch.long)

    train_data       = TensorDataset(all_token_ids, all_weight, all_tag_ids)
    train_sampler    = RandomSampler(train_data)
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=hps.batch_size)

    # training loop
    step = 0
    avg_losses = []
    for epoch in range(hps.max_epoch):
        for step, batch in enumerate(train_dataloader):
            batch = tuple(t.to(device) for t in batch)
            token_ids, weight, tag_ids = batch

            # init for updating the current batch
            optimizer.zero_grad()

            # forward pass
            loss = model(token_ids, weight, tag_ids, mode='train')
            
            # backward pass
            loss.backward() # accumulates the gradient (by addition) for each parameter. This is why you should call optimizer.zero_grad()

            # parameter update 
            optimizer.step()

            # monitoring at every 100 steps
            _loss = loss.item()  # loss.item() # gets the a scalar value held in the loss.
            avg_losses.append( _loss ) 
            if step % 100 == 0 :
                print('Epoch={} \t Step={} \t Loss={:.6f}'.format(
                            epoch, 
                            step,
                            np.mean(avg_losses)
                        )
                      )
                avg_losses = []
            step += 1

    # save model 
    torch.save(model, to_model_fn)
    print("Model saved at {}".format(to_model_fn) )



def train_crf_n2n(model, train_exs, hps, to_model_fn):
    model.train() # set information to pytorch that the current mode is 'training'
    
    # use gpu if set
    device = torch.device("cuda" if model.hps.use_gpu else "cpu")
    if device.type == 'cuda': model = model.cuda()

    # Loss and Optimizer
    criterion = nn.CrossEntropyLoss()  
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)  

    # training loop
    step = 0
    avg_losses = []

    # ---- dataset ---- #
    all_token_ids = torch.tensor([f.token_ids for f in train_exs], dtype=torch.long)
    all_weight    = torch.tensor([f.weight for f in train_exs], dtype=torch.float)
    all_tag_ids   = torch.tensor([f.tag_ids for f in train_exs], dtype=torch.long)

    train_data       = TensorDataset(all_token_ids, all_weight, all_tag_ids)
    train_sampler    = RandomSampler(train_data)
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=hps.batch_size)

    # training loop
    step = 0
    avg_losses = []
    for epoch in range(hps.max_epoch):
        for step, batch in enumerate(train_dataloader):
            batch = tuple(t.to(device) for t in batch)
            token_ids, weight, tag_ids = batch

            # init for updating the current batch
            optimizer.zero_grad()

            # forward pass
            logits = model(token_ids)

            # loss calculation considering padding symbols
            loss = model.get_crf_loss(logits, tag_ids, weight)

            # backward pass
            loss.backward() # accumulates the gradient (by addition) for each parameter. This is why you should call optimizer.zero_grad()

            # parameter update
            optimizer.step()

            # monitoring at every 100 steps
            _loss = loss.item()  # loss.item() # gets the a scalar value held in the loss.
            avg_losses.append( _loss ) 
            if step % 10 == 0 :
                print('Epoch={} \t Step={} \t Loss={:.6f}'.format(
                            epoch, 
                            step,
                            np.mean(avg_losses)
                        )
                      )
                avg_losses = []
            step += 1

    # save model 
    torch.save(model, to_model_fn)
    print("Model saved at {}".format(to_model_fn) )