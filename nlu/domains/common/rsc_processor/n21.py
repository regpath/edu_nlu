'''
Common N21 
    - resource related codes

Author : Sangkeun Jung (2019)
'''

import numpy as np
import random
import codecs

def load_text_rsc(fn):
    print("Data loading from {}".format(fn))

    # data format  
    # intent \t tokens 

    rsc = []
    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip()

            fields = line.split('\t')

            n21_class = fields[0]
            tokens = [ t for t in fields[1:] ]

            rsc.append( (n21_class, tokens) ) 
    return rsc 


def load_rsc(fn):
    # INPUT : id converted tab seperated format 
    print("Data loading from {}".format(fn))

    rsc = []
    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip()
            fields = line.split('\t')

            n21_class_id = int( fields[0] )
            token_ids = [ int(_id) for _id in fields[1:] ]

            rsc.append( (n21_class_id, token_ids) ) 

    return rsc 


def line_tensor_process(tokens, num_steps, pad_token_id=1, reverse=True):
    # input: id converted tokens
    token_ids = [ pad_token_id ] * num_steps
    weight    = [ 0 ] * num_steps

    for idx, _id in enumerate(tokens):
        token_ids[idx] = _id
        weight[idx] = 1 

    # why reverse?? --> for padding symbol processing
    if reverse == True:
        token_ids = list( reversed( token_ids ) ) 
        weight    = list( reversed( weight ) )    

    return token_ids, weight
          



class N21Example:
    def __init__(self, label_id, token_ids, weight):
        self.label_id = label_id
        self.token_ids = token_ids
        self.weight = weight


def make_examples(rsc, num_steps, pad_token_id=1):
    data = []
    for ex_id, (intent_id, tokens) in enumerate(rsc):
        # for tokens
        token_ids, weight = line_tensor_process(tokens, num_steps, pad_token_id, reverse=True)
        data.append( N21Example(intent_id, token_ids, weight) )
    return data




def load_n21_predicted_result_format(fn):
    rsc = []
    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip()
            fields = line.split('\t')

            # ref_id \t pred_id \t prob(float) \t token ids 
            ref_id = fields[0]
            pred_id = fields[1]
            prob = float(fields[2])
            tokens = fields[3:]

            rsc.append( (ref_id, pred_id, prob, tokens) )
    return rsc



def convert_a_sent_to_n21_example(text, token_vocab, num_steps, pad_token_id=1, token_unit='char'):
    # tokenize here
    if token_unit == 'char':
        tokens = list(text) # character as token
    else:
        print("NOT IMPLEMENTED YET !!!!")
        import sys; sys.exit()

    # to id
    tokens = [ token_vocab.get(t, token_vocab['_UNK']) for t in tokens  ]
    token_ids, weight = line_tensor_process(tokens, num_steps, pad_token_id, reverse=True)
            
    return N21Example(0, token_ids, weight)  # 0 for not-determined yet

