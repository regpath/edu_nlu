'''
Common N2N specific
    - resource related codes

Author : Sangkeun Jung (2019)
'''

import numpy as np
import random
import codecs



def load_text_rsc(fn):
    print("Data loading from {}".format(fn))

    # data format  
    # ex) -- \t tab spearated 
    #    BOS	O
    #    i	O
    #    want	O
    #    to	O
    #    fly	O
    #    from	O
    #    baltimore	B-fromloc.city_name
    #    to	O
    #    dallas	B-toloc.city_name
    #    round	B-round_trip
    #    trip	I-round_trip
    #    EOS	O
    #    --------------------------------------------------

    rsc = []
    with codecs.open(fn, 'r', encoding='utf-8') as f:
        a_sent = []
        for line in f:
            line = line.rstrip()

            if line.startswith('-'*10):
                rsc.append( a_sent ) 
                a_sent = []
                continue

            token, tag = line.split('\t')
            a_sent.append( (token, tag) ) 

    return rsc 

def load_rsc(fn):
    print("Data loading from {}".format(fn))

    # data format  (following is example. the real is converted as id)
    # ex) -- \t tab spearated 
    #    BOS	O
    #    i	O
    #    want	O
    #    to	O
    #    fly	O
    #    from	O
    #    baltimore	B-fromloc.city_name
    #    to	O
    #    dallas	B-toloc.city_name
    #    round	B-round_trip
    #    trip	I-round_trip
    #    EOS	O
    #    --------------------------------------------------

    rsc = []
    with codecs.open(fn, 'r', encoding='utf-8') as f:
        a_sent = []
        for line in f:
            line = line.rstrip()

            if line.startswith('-'*10):
                rsc.append( a_sent ) 
                a_sent = []
                continue

            token_id, tag_id = line.split('\t')
            token_id = int(token_id)
            tag_id   = int(tag_id) 

            a_sent.append( (token_id, tag_id) ) 

    return rsc 

class N2NExample:
    def __init__(self, token_ids, weight, tag_ids):
        self.token_ids = token_ids
        self.weight    = weight
        self.tag_ids   = tag_ids


def line_tensor_process(sent, num_steps, pad_token_id=1, o_tag_id=0):
    # input: id converted tokens
    tag_ids   = [ o_tag_id ] * num_steps
    token_ids = [ pad_token_id ] * num_steps
    weight    = [ 0 ] * num_steps

    for step_idx, (token_id, tag_id) in enumerate(sent):
        tag_ids[step_idx]   = tag_id
        token_ids[step_idx] = token_id
        weight[step_idx]    = 1 

    return token_ids, weight, tag_ids 
        
def make_examples(rsc, num_steps, pad_token_id=1, o_tag_id=0):
    data = []
    for ex_id, sent in enumerate(rsc):
        token_ids, weight, tag_ids = line_tensor_process(sent, num_steps, pad_token_id, o_tag_id)
        data.append( N2NExample(token_ids, weight, tag_ids) )
    return data

def convert_a_sent_to_n2n_example(text, token_vocab, num_steps, pad_token_id=1, o_tag_id=0, token_unit='char'):

    # tokenize here
    if token_unit == 'char':
        tokens = list(text) # character as token
    else:
        print("NOT IMPLEMENTED YET !!!!")
        import sys; sys.exit()

    # to id
    tokens = [ token_vocab.get(t, token_vocab['_UNK']) for t in tokens  ]

    # add null tag ids 
    sents = [ (t, 0) for t in tokens]  # 0 for dummy (meaningless) tag id 
    token_ids, weight, tag_ids = line_tensor_process(sents, num_steps, pad_token_id, o_tag_id)
        
    return N2NExample(token_ids, weight, tag_ids)



from random import randint

def ____make_tensor_data(rsc, num_steps, batch_size, pad_token_id=1, o_tag_id=0):
    num_exs = len(rsc) 

    n_to_fill = batch_size - num_exs % batch_size
    N = num_exs + n_to_fill

    # vectorize id data
    n_tags   = np.zeros([N, num_steps], np.int64)  
    n_token  = np.zeros([N, num_steps], np.int64)
    n_weight = np.zeros([N, num_steps], np.int64)

    for ex_id, sent in enumerate(rsc):
        tag_ids, token_ids, weight = line_tensor_process(sent, num_steps, pad_token_id, o_tag_id)
        
        n_tags[ex_id]    =  tag_ids   
        n_token[ex_id]   =  token_ids 
        n_weight[ex_id]  =  weight   
        
    # fill remaining batch
    random_idxs = [randint(0, num_exs-1) for p in range(0, n_to_fill)] 
    for _idx, random_index in enumerate( random_idxs ):
        idx = num_exs + _idx
        n_tags[idx]    =  n_tags[random_index]
        n_token[idx]   =  n_token[random_index]  
        n_weight[idx]  =  n_weight[random_index]

    tensors = n_tags, n_token, n_weight
    return tensors
    
def ____make_batch_data(rsc, batch_size, shuffle=True):
    # chunk the resource and make it as batch form
    #
    # rsc = list of (n_tags, n_token, n_weight)
    
    n_tags, n_token, n_weight = rsc 
    num_exs = n_tags.shape[0]
    indices = list( range(0, num_exs) )

    if shuffle == True:
        random.shuffle(indices)

    # batches
    batches = []
    for i in range(0, len(indices), batch_size):
        a_batch_idx = indices[i:i+batch_size]
        a_batch_tag, a_batch_token, a_batch_weight = rsc[0][a_batch_idx], rsc[1][a_batch_idx], rsc[2][a_batch_idx]
        batches.append( (a_batch_tag, a_batch_token, a_batch_weight) )

    return batches


def load_n2n_predicted_result_format(fn):
    rsc = []
    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip()
            fields = line.split('\t')

            sent = []
            for step in fields:
                id, prob = step.split(',')
                id = int(id)
                prob = float(prob)

                sent.append( (id, prob) )
            rsc.append(sent)
    return rsc



