## Natural Language Understanding using Deep Learning
##
## this code is written for educational purpose


### Author : Sangkeun Jung (hugman@cnu.ac.kr)



### Working Process


0. prepare data
	0.1 : download data (*.xlsx file from web or repo.)
	0.2 : convert data to raw.text 
		python convert_excel_to_tagged_text.py /YOUR_SOURCE_FOLDDER/source.xlsx /TO_TARGET_FOLDER/raw.text

	0.3 : convert xml data to bio-tagged data with tokenization
		python convert_xml_to_bio.py

	0.4 : collect vocabs from the train and text data (from tokenized)
		collect_vocab.py
	
	0.5 : convert text data to id data with vocabulary 
		convert_text_to_id.py



	