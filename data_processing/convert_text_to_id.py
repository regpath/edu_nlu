"""
    convert text file to id file using vocab file


    INPUT : text.n21.txt and text.n2n.text, *.vocab
    OUTPUT : 
            - token.vocab
            - n21.vocab
            - n2n.vocab
"""


import codecs, os 
import collections 

def load_n21(fn):

    class_vocabs = collections.Counter()
    tokens = collections.Counter()

    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip()
            
            fields = line.split('\t')

            sent_class = fields[0]
            _tokens = fields[1:]

            class_vocabs[sent_class] += 1
            for t in _tokens:
                tokens[t] += 1 

    return class_vocabs, tokens


def load_n2n(fn):

    class_vocabs = collections.Counter()
    tokens = collections.Counter()

    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip()

            if line.startswith('-'*20):
                # end of sentence
                continue 
            
            token, token_class = line.split('\t')

            class_vocabs[token_class] += 1
            tokens[token] += 1 

    return class_vocabs, tokens

def load_vocab(fn):
    vocab = {}
    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip()

            symbol, id = line.split('\t') 
            id = int(id) 

            vocab[symbol] = id

    return vocab 


def convert_n21(in_fn, token_vocab, n21_class_vocab, to_fn):

    with codecs.open(to_fn, 'w', encoding='utf-8') as of:
        with codecs.open(in_fn, 'r', encoding='utf-8') as f:
            for line in f:
                line = line.rstrip()
            
                fields = line.split('\t')

                sent_class = fields[0]
                _tokens = fields[1:]

                # convert
                sent_class_id = n21_class_vocab[sent_class]
                token_ids = [ str(token_vocab[t]) for t in _tokens]

                # dump
                print("{}\t{}".format(sent_class_id, "\t".join(token_ids) ), file=of)

    print("Converted file is dumped at {}".format(to_fn) )


def convert_n2n(in_fn, token_vocab, token_class_vocab, to_fn):

    with codecs.open(to_fn, 'w', encoding='utf-8') as of:
        with codecs.open(in_fn, 'r', encoding='utf-8') as f:
            for line in f:
                line = line.rstrip()

                if line.startswith('-'*20):
                    # end of sentence
                    print("-"*50, file=of)
                    continue 
            
                token, token_class = line.split('\t')

                # conert 
                token_class_id = token_class_vocab[token_class]
                token_id = token_vocab[token]

                # dump
                print("{}\t{}".format(token_id, token_class_id), file=of)

    print("Converted file is dumped at {}".format(to_fn) )

def convert_files(fns):
    
    token_vocab     = load_vocab(fns['vocab']['token'] )
    n21_class_vocab = load_vocab(fns['vocab']['n21'] )
    n2n_class_vocab = load_vocab(fns['vocab']['n2n'] )

    # n21 (train, test)
    convert_n21(fns['from']['n21']['train'], token_vocab, n21_class_vocab, fns['to']['n21']['train'])
    convert_n21(fns['from']['n21']['test'], token_vocab, n21_class_vocab, fns['to']['n21']['test'])

    # n2n (train, test)
    convert_n2n(fns['from']['n2n']['train'], token_vocab, n2n_class_vocab, fns['to']['n2n']['train'])
    convert_n2n(fns['from']['n2n']['test'], token_vocab, n2n_class_vocab, fns['to']['n2n']['test'])

# set current SCRIPT file directory as a working directory
os.chdir( os.path.dirname( os.path.abspath(__file__) ) )

if __name__ == '__main__':
    
    domain = 'weather'
    data_root = "../data"


    data_folder = os.path.join(data_root, domain, "tokenized")
    vocab_folder = os.path.join(data_root, domain, "vocab")
    id_folder = os.path.join(data_root, domain, "id_converted") 

    if not os.path.exists(id_folder): os.makedirs(id_folder)


    fns = {
            'from' : {
                'n21' : {
                    'train'  : os.path.join(data_folder, 'train.text.n21.txt'),
                    'test'   : os.path.join(data_folder, 'test.text.n21.txt'), 
                    },
                'n2n' : {
                    'train' : os.path.join(data_folder, 'train.text.n2n.txt'),
                    'test'   : os.path.join(data_folder, 'test.text.n2n.txt'), 
                    }
            },
            'to' : {
                'n21' : {
                    'train'  : os.path.join(id_folder, 'train.id.n21.txt'),
                    'test'   : os.path.join(id_folder, 'test.id.n21.txt'), 
                    },
                'n2n' : {
                    'train' : os.path.join(id_folder, 'train.id.n2n.txt'),
                    'test'   : os.path.join(id_folder, 'test.id.n2n.txt'), 
                    }
            },
            'vocab':{
                'token'  : os.path.join(vocab_folder, 'token.vocab'),
                'n21'   : os.path.join(vocab_folder, 'n21.class.voab'), 
                'n2n'   : os.path.join(vocab_folder, 'n2n.class.voab'), 
                }
          }

    convert_files(fns)





