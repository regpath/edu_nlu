"""
    collect vocabs from text.n21 and text.n2n formats 

        - includes
                - collect vocab 
                - dump vocab


    INPUT : *tokenized* text.n21.txt and text.n2n.text
    OUTPUT : 
            - token.vocab
            - n21.vocab
            - n2n.vocab
"""


import codecs, os 
import collections 
os.chdir( os.path.dirname( os.path.abspath(__file__ ) ) )

def load_n21(fn):

    class_vocabs = collections.Counter()
    tokens = collections.Counter()

    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip()
            
            fields = line.split('\t')

            sent_class = fields[0]
            _tokens = fields[1:]

            class_vocabs[sent_class] += 1
            for t in _tokens:
                tokens[t] += 1 

    return class_vocabs, tokens


def load_n2n(fn):

    class_vocabs = collections.Counter()
    tokens = collections.Counter()

    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.rstrip()

            if line.startswith('-'*20):
                # end of sentence
                continue 
            
            token, token_class = line.split('\t')

            class_vocabs[token_class] += 1
            tokens[token] += 1 

    return class_vocabs, tokens


def collect_and_dump_vocab(fns):
    
    # load n21
    n21_train_classes, n21_train_tokens = load_n21( fns['n21']['train'] )
    n21_test_classes,  n21_test_tokens  = load_n21( fns['n21']['test'] )
    n21_classes = n21_train_classes + n21_test_classes
    n21_tokens = n21_train_tokens + n21_test_tokens
    

    # load n2n
    n2n_train_classes, n2n_train_tokens = load_n2n( fns['n2n']['train'] )
    n2n_test_classes, n2n_test_tokens   = load_n2n( fns['n2n']['test'] )
    n2n_classes = n2n_train_classes + n2n_test_classes
    n2n_tokens  = n2n_train_tokens + n2n_test_tokens



    def dump_vocab_file(vocab, to_fn, reserved_vocab=None):
        _dir = os.path.dirname(to_fn)
        if not os.path.exists(_dir): os.makedirs(_dir)

        if reserved_vocab: 
            out_vocabs = reserved_vocab
        else:
            out_vocabs = [] 
        reserved_index = len( out_vocabs )

        for idx, (k, v) in enumerate( vocab.most_common() ):
            out_vocabs.append( (k, idx+reserved_index) ) 

        with codecs.open(to_fn, 'w', encoding='utf-8') as f:
            for k, v in out_vocabs:
                print('{}\t{}'.format(k,v), file=f)

        print("Vocab file is dumped at {}".format(to_fn) ) 


    # dump 
    dump_vocab_file(n21_tokens,  fns['output']['token'], reserved_vocab=[ ('_UNK', 0), ('_PAD', 1) ])
    dump_vocab_file(n21_classes, fns['output']['n21'])
    dump_vocab_file(n2n_classes, fns['output']['n2n'])



if __name__ == '__main__':
    domain = 'weather'
    
    data_root = '../data'

    data_folder = os.path.join(data_root, domain, 'tokenized')
    output_data_folder = os.path.join(data_root, domain, 'vocab')

    fns = {
            'n21' : {
                'train' : os.path.join(data_folder, 'train.text.n21.txt'),
                'test'   : os.path.join(data_folder, 'test.text.n21.txt'), 
                },
            'n2n' : {
                'train' : os.path.join(data_folder, 'train.text.n2n.txt'),
                'test'   : os.path.join(data_folder, 'test.text.n2n.txt'), 
                },

            'output':{
                'token'  : os.path.join(output_data_folder, 'token.vocab'),
                'n21'   : os.path.join(output_data_folder, 'n21.class.voab'), 
                'n2n'   : os.path.join(output_data_folder, 'n2n.class.voab'), 
                }
          }

    collect_and_dump_vocab(fns)





