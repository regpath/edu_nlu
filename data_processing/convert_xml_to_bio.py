"""
    Convert XML tagged text to tokenized BIO tagged text (n21, n2n format)

    In this script, the corpus is shuffled and splited to train / test data
"""

import os, sys
os.chdir( os.path.dirname( os.path.abspath(__file__ ) ) )

from converter.xml_to_bio import convert as xml_to_bio

import codecs
def convert(fn, to_n21, to_n2n, split_rate=0.7):
    # input : filename of xml-like tagged nlu corpus
    # output : bio tagged corpus (train / test split)

    sents = []
    with codecs.open(fn, 'r', encoding='utf-8') as f:
        for line_num, line in enumerate(f):
            line = line.rstrip()
            

            intent, tagged_text = line.split('\t')

            character_bio = xml_to_bio(tagged_text)

            # (TODO) 
            # tokenizer and alignment should be implemented in here for supporting word-piece model 
            item = (intent, character_bio)
            sents.append( item ) 

    # shuffle and split
    import random
    random.shuffle(sents)

    split_index = int( len(sents) * split_rate )
    train_data, test_data = sents[:split_index], sents[split_index:]




    # dump
    def dump_to_file(sents_data, n21_fn, n2n_fn):
        _dir = os.path.dirname(n21_fn)
        if not os.path.exists(_dir): os.makedirs(_dir)

        n21_f = codecs.open(n21_fn, 'w', encoding='utf-8')
        n2n_f = codecs.open(n2n_fn, 'w', encoding='utf-8')

        for item in sents_data:
            intent, tokenized_bio = item

            tokens = tokenized_bio[0]
            tokenized_text = "\t".join(tokens)

            # to n21
            print("{}\t{}".format(intent, tokenized_text), file=n21_f)

            # to n2n
            for token, tag in zip(tokenized_bio[0], tokenized_bio[1]):
                print("{}\t{}".format(token, tag), file=n2n_f)

            print("-"*50, file=n2n_f)
    
        n21_f.close()
        n2n_f.close()

        print("N21 Tokenized BIO text file is dumped at ", to_n21)
        print("N2N Tokenized BIO text file is dumped at ", to_n2n)

    # train data 
    dump_to_file(train_data, to_n21['train'], to_n2n['train'])
    dump_to_file(test_data, to_n21['test'], to_n2n['test'])


if __name__ == '__main__':
    
    domain = 'weather' 
    data_root = '../data'

    fn = os.path.join(data_root, data_root, domain, "raw", 'raw.txt')

    data_folder = os.path.join(data_root, domain)
    to_tokenized_n21 = {
                            'train': os.path.join(data_folder, "tokenized", "train.text.n21.txt"),
                            'test' : os.path.join(data_folder, "tokenized", "test.text.n21.txt")
                       }
    to_tokenized_n2n = {
                            'train': os.path.join(data_folder, "tokenized", "train.text.n2n.txt"),
                            'test' : os.path.join(data_folder, "tokenized", "test.text.n2n.txt")
                       }
    
    convert(fn, to_tokenized_n21, to_tokenized_n2n, split_rate=0.7)





